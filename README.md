# Search bar with live search

## Description

A search bar made with HTML5, CSS3 and JS which shows highlighted matches upon searching a country name.

**Project video**
https://drive.google.com/file/d/1vE2Q3_BiTHvXu41nuW9RybdYD0wXozMU/view?usp=sharing

## Screenshots

With no text
![No search text](/screenshots/Search-empty.png)

When searched for a country
![Searching country name](/screenshots/Search-text.png)
