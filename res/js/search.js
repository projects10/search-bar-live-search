const searchBox = document.querySelector('.searchBox');
const suggestionsDiv = document.querySelector('.suggestions');

const countries = [];

fetch('https://restcountries.eu/rest/v2/all')
  .then((response) => response.json())
  .then((data) => {
    countries.push(...data);
  });

// Search by country name or capital
const displayResults = function () {
  // if search bar empty, exit from function
  if (this.value === '') {
    suggestionsDiv.innerHTML = '';
    suggestionsDiv.classList.add('hidden')
    return;
  }

  suggestionsDiv.classList.remove('hidden')

  // filter countries
  let filteredCountry = countries.filter((country) => {
    return country.name.toLowerCase().includes(this.value.toLowerCase());
  });

  // create array of list items for each filtered country
  let listItems = filteredCountry.map((country) => {
    let regex = new RegExp(this.value, 'gi');
    return `<li>${country.name}</li>`.replace(
      regex,
      "<span class='highlight'>$&</span>"
    );
  });

  // show the results
  suggestionsDiv.innerHTML = listItems.join('');
};

searchBox.addEventListener('input', displayResults);
